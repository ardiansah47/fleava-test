import {tns} from "./plugin/tiny-slider.js";
var skrollTop = require("./plugin/skrollTop");
require("./plugin/easings");
import AOS from 'aos';

AOS.init();

var slider = tns({
    container: '.slider',
    items: 3,
    slideBy: 'page',
    edgePadding: 150,
    prevButton: false,
    mouseDrag: true,
    responsive: {
      1920:{
      	items: 5
      },
	  1400:{
	  	items: 4
	  },
	  900:{
		items: 3
	  },
      768: {
        items: 2
      },
      640: {
        items: 2,
        edgePadding: 100,
        gutter:15
      },
      480:{
      	edgePadding: 40
      },
      425:{
      	edgePadding: 0,
      	gutter: 0
      },
      300:{
      	items: 1,
      	edgePadding: 0,
      	mouseDrag: true
      }
     
    }
});

var new_scroll_position = 0;
var last_scroll_position;
var header = document.getElementById("header");

window.addEventListener('scroll', function(e) {
  last_scroll_position = window.scrollY;

  if (new_scroll_position < last_scroll_position && last_scroll_position > 10) {
    header.classList.remove("slideDown");
    header.classList.add("slideUp");

  } else if (new_scroll_position > last_scroll_position) {
    header.classList.remove("slideUp");
    header.classList.add("slideDown");
  }

  new_scroll_position = last_scroll_position;

  if(last_scroll_position == 0){
  	header.classList.remove("slideDown");
  }
});

var offsets = document.getElementById('section-second').getBoundingClientRect();
var top     = offsets.top;

document.getElementById('scroll-down').addEventListener('click', function(){
    skrollTop.scrollTo({
        to: top,
        easing: window.easings.easeInOutQuad,
        duration: 600,
        callback: function() {
           console.log("finished!");
        }
    });
});
