## Quick Start 
Follow the instructions below :
1. Clone this repository ``` git clone https://gitlab.com/ardiansah47/fleava-test.git ```
2. And enter the folder ``` cd fleava-test ```
3. Run ``` npm install ```
4. After it finishes run ``` npm run dev ``` for compiling the assets
5. Your project will run in ``` http://localhost:3000 ```
## Includes
1. BrowserSync
2. Nunjucks
3. Scss 
## Get in touch
* [Indra Ardiansah](https://www.linkedin.com/in/indra-a-b62aa3113/)

